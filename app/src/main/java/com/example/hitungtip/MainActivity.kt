package com.example.hitungtip

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.hitungtip.databinding.ActivityMainBinding
import java.text.NumberFormat


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.calculateButton.setOnClickListener { calculateTip() }
    }

    private fun calculateTip() {
        val stringInTextField = binding.costOfService.text.toString()
        val cost = stringInTextField.toDoubleOrNull()
        if (cost == null) {
            binding.tipResult.text = ""
            return
        }

        val tipPercentage = when (binding.tipOptions.checkedRadioButtonId) {
            R.id.option_satu_percent -> {
                0.20
            }
            R.id.option_dua_percent -> {
                0.18
            }
            R.id.option_tiga_percent -> {
                0.15
            }
            else -> {
                0.10
            }
        }

        var tip = tipPercentage * cost
        if (binding.roundUpSwitch.isChecked) tip = kotlin.math.ceil(tip)

        val formattedTip = NumberFormat.getCurrencyInstance().format(tip)
        binding.tipResult.text = getString(R.string.tip_amount, formattedTip)
    }
}